# Ansible Role: Elasticsearch


# MY CHANGES
We need to change couple of things to install different version of elasticsearch package. Example for elasticsearch 6.3.2 we need to change
following files:
geerlingguy.elasticsearch role, which is also the name of folder, which  would be relative path for following changes
apsolute path:  /home/name_of_user/.ansible/roles/geerlingguy.elasticsearch/tasks
relative path: 

./tasks/setup-Debian.yml
OR if you need instalation on REdHat and derivates
./tasks/setup-RedHat.yml
Example for setup-Debian.yml

change:  repo: 'deb https://artifacts.elastic.co/packages/6.x/apt stable main'
AND/OR
url: https://packages.elastic.co/GPG-KEY-elasticsearch

Also for RedHat and derivates we need to change inside

./files/elasticsearch.repo

[elasticsearch-6.x]
name=Elasticsearch repository for 6.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1

For this role we need to install also role gerlingguy.java

ansible-galaxy install gerlingguy.java

 
[![Build Status](https://travis-ci.org/geerlingguy/ansible-role-elasticsearch.svg?branch=master)](https://travis-ci.org/geerlingguy/ansible-role-elasticsearch)

An Ansible Role that installs Elasticsearch on RedHat/CentOS or Debian/Ubuntu.

## Requirements

Requires at least Java 7 (Java 8+ preferred). See [`geerlingguy.java`](https://github.com/geerlingguy/ansible-role-java#example-playbook-install-openjdk-8) role instructions for installing OpenJDK 8.

## Role Variables
Available variables are listed below, along with default values (see `defaults/main.yml`):

    elasticsearch_network_host: localhost

Network host to listen for incoming connections on. By default we only listen on the localhost interface. Change this to the IP address to listen on a specific interface, or `0.0.0.0` to listen on all interfaces.

    elasticsearch_http_port: 9200

The port to listen for HTTP connections on.

    elasticsearch_script_inline: true
    elasticsearch_script_indexed: true

Whether to allow inline scripting against ElasticSearch. You should read the following link as there are possible security implications for enabling these options: [Enable Dynamic Scripting](https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-scripting.html#enable-dynamic-scripting). Available options include: `true`, `false`, and `sandbox`.

## Dependencies

  - geerlingguy.java

## Example Playbook

    - hosts: search
      roles:
        - geerlingguy.java
        - geerlingguy.elasticsearch

## License

MIT / BSD

## Author Information

This role was created in 2014 by [Jeff Geerling](https://www.jeffgeerling.com/), author of [Ansible for DevOps](https://www.ansiblefordevops.com/).
